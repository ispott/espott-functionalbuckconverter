<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1" unitdist="mm" unit="mm" style="lines" multiple="1" display="yes" altdistance="0.1" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LM2576T">
<packages>
<package name="T05A_TEX">
<pad name="1" x="0" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="2" x="1.7018" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="3" x="3.4036" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="4" x="5.1054" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="5" x="6.8072" y="0" drill="1.0922" diameter="1.6002"/>
<wire x1="-1.9812" y1="-1.9812" x2="8.7884" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="-1.9812" x2="8.7884" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="2.9972" x2="-1.9812" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="2.9972" x2="-1.9812" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="0" x2="-3.9624" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.9624" y1="0" x2="-3.556" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="-0.254" x2="0" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="-1.8288" x2="8.6868" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="-1.8288" x2="8.6868" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="2.8448" x2="-1.8796" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="2.8448" x2="-1.8796" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="-1.1684" y1="0" x2="-1.5748" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.5748" y1="0" x2="-1.1684" y2="0" width="0" layer="51" curve="-180"/>
<text x="0.127" y="-0.127" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="1.6764" y="-0.127" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="LM2576T-5_0_1">
<pin name="VIN" x="2.54" y="0" length="middle" direction="pwr"/>
<pin name="OUT" x="2.54" y="-2.54" length="middle" direction="pwr"/>
<pin name="GND" x="63.5" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="FB" x="63.5" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="~ON~/OFF" x="63.5" y="0" length="middle" direction="in" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="58.42" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-10.16" x2="58.42" y2="5.08" width="0.1524" layer="94"/>
<wire x1="58.42" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="28.2956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="27.6606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM2576T-ADJ/LF03" prefix="U">
<gates>
<gate name="A" symbol="LM2576T-5_0_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T05A_TEX">
<connects>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="3"/>
<connect gate="A" pin="OUT" pad="2"/>
<connect gate="A" pin="VIN" pad="1"/>
<connect gate="A" pin="~ON~/OFF" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="296-35127-5-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-LM2576T-ADJ/LF03-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LM2576T-ADJ/LF03" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Lab6_Library">
<packages>
<package name="CPF11K0000FKEE6-R1">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="7" y="0" drill="0.7"/>
<wire x1="5.715" y1="0" x2="7" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.715" y2="0" width="0.127" layer="21"/>
<text x="1.651" y="1.778" size="1.27" layer="25">CPF1</text>
<text x="0.762" y="-2.921" size="1.27" layer="27">1k Ohm</text>
</package>
<package name="M10144">
<pad name="P$1" x="0" y="0" drill="0.6"/>
<pad name="P$2" x="7.5" y="0" drill="0.6"/>
<wire x1="0" y1="0" x2="1.5" y2="0" width="0.127" layer="21"/>
<wire x1="7.5" y1="0" x2="6.7" y2="0" width="0.127" layer="21"/>
<wire x1="1.5" y1="0" x2="2.4" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="2.4" y1="0.9" x2="2.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="2.5" y1="0.9" x2="3.2" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="3.2" y1="0.2" x2="3.2" y2="0" width="0.127" layer="21"/>
<wire x1="3.2" y1="0" x2="4.1" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="4.1" y1="0.9" x2="4.2" y2="0.9" width="0.127" layer="21"/>
<wire x1="4.2" y1="0.9" x2="4.8" y2="0.3" width="0.127" layer="21" curve="-90"/>
<wire x1="4.8" y1="0.3" x2="4.8" y2="0" width="0.127" layer="21"/>
<wire x1="4.8" y1="0" x2="5.7" y2="0.9" width="0.127" layer="21" curve="-90"/>
<wire x1="5.7" y1="0.9" x2="6" y2="0.9" width="0.127" layer="21"/>
<wire x1="6" y1="0.9" x2="6.7" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="6.7" y1="0.2" x2="6.7" y2="0" width="0.127" layer="21"/>
<text x="0.8" y="1.6" size="1.27" layer="25">M10144</text>
<text x="2.1" y="-1.9" size="1.27" layer="27">68uH</text>
</package>
<package name="NF123G-302">
<pad name="P$1" x="0" y="0" drill="1"/>
<pad name="P$2" x="12.5" y="0" drill="1"/>
<wire x1="0" y1="0" x2="2" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="4" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="4" y2="2" width="0.127" layer="21"/>
<wire x1="12.5" y1="0" x2="10.5" y2="0" width="0.127" layer="21"/>
<wire x1="10.5" y1="0" x2="10.5" y2="2" width="0.127" layer="21"/>
<wire x1="10.5" y1="2" x2="9" y2="2" width="0.127" layer="21"/>
<wire x1="10.5" y1="0" x2="10.5" y2="-2" width="0.127" layer="21"/>
<wire x1="10.5" y1="-2" x2="9" y2="-2" width="0.127" layer="21"/>
<circle x="6.5" y="0" radius="10.01798125" width="0.127" layer="21"/>
<circle x="6.5" y="0" radius="3.11448125" width="0.127" layer="21"/>
<text x="5.8" y="-0.6" size="1.27" layer="21">M</text>
</package>
<package name="1N5820">
<pad name="P$1" x="0" y="0" drill="1.3"/>
<pad name="P$2" x="9" y="0" drill="1.3"/>
<wire x1="0" y1="0" x2="4" y2="0" width="0.127" layer="21"/>
<wire x1="4" y1="0" x2="4" y2="2" width="0.127" layer="21"/>
<wire x1="4" y1="2" x2="6" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="4" y2="-2" width="0.127" layer="21"/>
<wire x1="4" y1="-2" x2="4" y2="0" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="2" width="0.127" layer="21"/>
<wire x1="6" y1="2" x2="5.3" y2="2" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="-2" width="0.127" layer="21"/>
<wire x1="6" y1="-2" x2="6.6" y2="-2" width="0.127" layer="21"/>
<wire x1="6.6" y1="-2" x2="6.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="6" y1="0" x2="9" y2="0" width="0.127" layer="21"/>
<wire x1="5.3" y1="2" x2="5.3" y2="1.5" width="0.127" layer="21"/>
<text x="1" y="2.7" size="1.27" layer="25">1N5820</text>
<text x="2.032" y="0.635" size="1.27" layer="21">+</text>
<text x="7.366" y="0.762" size="1.27" layer="21">-</text>
</package>
<package name="1572-1039-CIN">
<pad name="P$1" x="0" y="0" drill="1"/>
<pad name="P$2" x="2" y="0" drill="1"/>
<circle x="1" y="0" radius="5" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0.6" y2="0" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="0.6" y2="1" width="0.127" layer="21"/>
<wire x1="0.6" y1="0" x2="0.6" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="1" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="0" x2="2" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="0" x2="2" y2="0" width="0.127" layer="21"/>
<text x="0" y="0.8" size="0.6096" layer="21">+</text>
</package>
<package name="1572-1055-COUT">
<pad name="P$1" x="0" y="0" drill="0.9"/>
<pad name="P$2" x="8" y="0" drill="0.9"/>
<circle x="4" y="0" radius="8" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.4" y2="0" width="0.127" layer="21"/>
<wire x1="1.5" y1="0" x2="3" y2="0" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="3" y2="-1" width="0.127" layer="21"/>
<wire x1="3" y1="0" x2="3" y2="1" width="0.127" layer="21"/>
<wire x1="4.6" y1="-1" x2="3.6" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="3.6" y1="0" x2="4.6" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="3.6" y1="0" x2="8.1" y2="0" width="0.127" layer="21"/>
<text x="1.5" y="0.4" size="1.27" layer="21">+</text>
</package>
<package name="CF18JT6K20CT">
<pad name="P$1" x="0" y="0" drill="0.5"/>
<pad name="P$2" x="4" y="0" drill="0.5"/>
<wire x1="0" y1="0" x2="0.5" y2="0" width="0.127" layer="21"/>
<wire x1="0.5" y1="0" x2="1" y2="1" width="0.127" layer="21"/>
<wire x1="1" y1="1" x2="1.5" y2="-1" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="2" y1="1" x2="2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="2.5" y1="-1" x2="3" y2="1" width="0.127" layer="21"/>
<wire x1="3" y1="1" x2="3.4" y2="0" width="0.127" layer="21"/>
<wire x1="3.4" y1="0" x2="4" y2="0" width="0.127" layer="21"/>
</package>
<package name="+-">
<pad name="P$1" x="0" y="0" drill="1"/>
<pad name="P$2" x="5" y="0" drill="1"/>
<text x="-1.27" y="0.508" size="1.27" layer="25">+</text>
<text x="3.937" y="0.508" size="1.27" layer="25">-</text>
</package>
</packages>
<symbols>
<symbol name="CPF11K0000FKEE6-R1">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="1.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="1.5" y1="0.9" x2="2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="-1" x2="2.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="2.5" y1="0.9" x2="3" y2="-1" width="0.254" layer="94"/>
<wire x1="3" y1="-1" x2="3.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="3.5" y1="0.9" x2="4" y2="-1" width="0.254" layer="94"/>
<wire x1="4" y1="-1" x2="4.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="4.5" y1="0.9" x2="4.9" y2="0" width="0.254" layer="94"/>
<wire x1="4.9" y1="0" x2="6.1" y2="0" width="0.254" layer="94"/>
<text x="0.8" y="1.5" size="1.27" layer="95">CPF1</text>
<text x="-0.1" y="-2.6" size="1.27" layer="96">1k Ohm</text>
<pin name="P$1" x="-4" y="0" length="middle"/>
<pin name="P$2" x="10" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="M10144">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="3" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="3" y1="0" x2="5" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="5" y1="0" x2="7" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="7" y1="0" x2="8" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="12" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="-4" y="0" length="middle"/>
<text x="1" y="1.7" size="1.27" layer="95">M10144</text>
<text x="2" y="-3" size="1.27" layer="96">68uH</text>
</symbol>
<symbol name="NF123G-302">
<wire x1="-2" y1="1" x2="-4" y2="1" width="0.254" layer="94"/>
<wire x1="-4" y1="1" x2="-4" y2="-1" width="0.254" layer="94"/>
<wire x1="-4" y1="-1" x2="-2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="1" x2="4" y2="1" width="0.254" layer="94"/>
<wire x1="4" y1="1" x2="4" y2="-1" width="0.254" layer="94"/>
<wire x1="4" y1="-1" x2="2" y2="-1" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.236065625" width="0.254" layer="94"/>
<text x="-1" y="-0.8" size="1.778" layer="94">M</text>
<pin name="P$1" x="-9" y="0" length="middle"/>
<pin name="P$2" x="9" y="0" length="middle" rot="R180"/>
<text x="-3.5" y="-0.6" size="1.27" layer="94">+</text>
<text x="2.7" y="-0.5" size="1.27" layer="94">-</text>
</symbol>
<symbol name="1N5820">
<wire x1="0" y1="0" x2="3" y2="0" width="0.254" layer="94"/>
<wire x1="3" y1="0" x2="3" y2="2" width="0.254" layer="94"/>
<wire x1="3" y1="0" x2="3" y2="-2" width="0.254" layer="94"/>
<wire x1="3" y1="-2" x2="5" y2="0" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="3" y2="2" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="5" y2="2" width="0.254" layer="94"/>
<wire x1="5" y1="2" x2="4.3" y2="2" width="0.254" layer="94"/>
<wire x1="4.3" y1="2" x2="4.3" y2="1.6" width="0.254" layer="94"/>
<wire x1="5" y1="0" x2="5" y2="-2" width="0.254" layer="94"/>
<wire x1="5" y1="-2" x2="5.8" y2="-2" width="0.254" layer="94"/>
<wire x1="5.8" y1="-2" x2="5.8" y2="-1.3" width="0.254" layer="94"/>
<pin name="P$1" x="-2" y="0" length="middle"/>
<pin name="P$2" x="10" y="0" length="middle" rot="R180"/>
<text x="1.5" y="0.4" size="1.27" layer="94">+</text>
<text x="5.7" y="0.5" size="1.27" layer="94">-</text>
</symbol>
<symbol name="1572-1039-CIN">
<wire x1="0" y1="0" x2="2" y2="0" width="0.254" layer="94"/>
<wire x1="2" y1="0" x2="2" y2="2" width="0.254" layer="94"/>
<wire x1="2" y1="-2" x2="2" y2="0" width="0.254" layer="94"/>
<wire x1="5" y1="-2" x2="5" y2="2.1" width="0.254" layer="94" curve="-180"/>
<text x="0.5" y="0.4" size="1.27" layer="94">+</text>
<pin name="P$1" x="-3" y="0" length="middle"/>
<pin name="P$2" x="8" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="1572-1055-COUT">
<wire x1="0" y1="-2" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2" width="0.254" layer="94"/>
<wire x1="3" y1="2" x2="3" y2="-2" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="0" x2="-1" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-5" y="0" length="middle"/>
<pin name="P$2" x="6" y="0" length="middle" rot="R180"/>
<text x="-1.4" y="0.4" size="1.27" layer="94">+</text>
</symbol>
<symbol name="CF18JT6K20CT-R2">
<wire x1="0" y1="0" x2="1" y2="0" width="0.254" layer="94"/>
<wire x1="1" y1="0" x2="1.5" y2="0.9" width="0.254" layer="94"/>
<wire x1="1.5" y1="0.9" x2="2" y2="-1" width="0.254" layer="94"/>
<wire x1="2" y1="-1" x2="2.6" y2="0.9" width="0.254" layer="94"/>
<wire x1="2.6" y1="0.9" x2="3" y2="-1" width="0.254" layer="94"/>
<wire x1="3" y1="-1" x2="3.5" y2="0.8" width="0.254" layer="94"/>
<wire x1="3.5" y1="0.8" x2="4" y2="0" width="0.254" layer="94"/>
<wire x1="4" y1="0" x2="5" y2="0" width="0.254" layer="94"/>
<pin name="P$1" x="-4" y="0" length="middle"/>
<pin name="P$2" x="9" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="+">
<pin name="P$1" x="-6" y="0" length="middle"/>
<pin name="P$2" x="5" y="0" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CPF11K0000KFEE6-R1">
<gates>
<gate name="G$1" symbol="CPF11K0000FKEE6-R1" x="-3.048" y="-0.254"/>
</gates>
<devices>
<device name="" package="CPF11K0000FKEE6-R1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M10144">
<gates>
<gate name="G$1" symbol="M10144" x="-4" y="0.1"/>
</gates>
<devices>
<device name="" package="M10144">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NF123G-302">
<gates>
<gate name="G$1" symbol="NF123G-302" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NF123G-302">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1N5820">
<gates>
<gate name="G$1" symbol="1N5820" x="-4" y="0"/>
</gates>
<devices>
<device name="" package="1N5820">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1572-1039-CIN">
<gates>
<gate name="G$1" symbol="1572-1039-CIN" x="-2" y="0"/>
</gates>
<devices>
<device name="" package="1572-1039-CIN">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1572-1055-COUT">
<gates>
<gate name="G$1" symbol="1572-1055-COUT" x="-0.9" y="0"/>
</gates>
<devices>
<device name="" package="1572-1055-COUT">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CF18JT6K20CT">
<gates>
<gate name="G$1" symbol="CF18JT6K20CT-R2" x="-2.6" y="-0.2"/>
</gates>
<devices>
<device name="" package="CF18JT6K20CT">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+-">
<gates>
<gate name="G$1" symbol="+" x="0" y="0"/>
</gates>
<devices>
<device name="" package="+-">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:39417/1" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground Symbol)&lt;/h3&gt;</description>
<pin name="3.3V" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="0" y="-1.778" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND2" urn="urn:adsk.eagle:component:39442/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply (Earth Ground style)&lt;/h3&gt;
&lt;p&gt;Ground supply with a traditional "earth ground" symbol.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="2.54" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="LM2576T" deviceset="LM2576T-ADJ/LF03" device=""/>
<part name="U$5" library="Lab6_Library" deviceset="CPF11K0000KFEE6-R1" device=""/>
<part name="U$6" library="Lab6_Library" deviceset="M10144" device=""/>
<part name="U$7" library="Lab6_Library" deviceset="NF123G-302" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="1N5820" library="Lab6_Library" deviceset="1N5820" device=""/>
<part name="1572-1039" library="Lab6_Library" deviceset="1572-1039-CIN" device=""/>
<part name="U$3" library="Lab6_Library" deviceset="1572-1055-COUT" device=""/>
<part name="CF18" library="Lab6_Library" deviceset="CF18JT6K20CT" device=""/>
<part name="U$1" library="Lab6_Library" deviceset="+-" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="6.1" y="-16.5" size="1.778" layer="95">1N5820</text>
<text x="-18.4" y="9.8" size="1.778" layer="95">1572-1039</text>
<text x="28.9" y="-10.3" size="1.778" layer="95">1572-1055</text>
<text x="-15.8" y="7.4" size="1.778" layer="96">100uF</text>
<text x="30.4" y="-13.1" size="1.778" layer="96">1000uF</text>
<text x="83" y="-15.8" size="1.778" layer="96">9V/GND</text>
<text x="-16.4" y="19.3" size="1.778" layer="96">12V</text>
<text x="80" y="-7" size="1.27" layer="95">CF18</text>
<text x="79.3" y="-2.5" size="1.27" layer="96">6k Ohm</text>
</plain>
<instances>
<instance part="U1" gate="A" x="1" y="19" smashed="yes">
<attribute name="NAME" x="29.2956" y="28.1186" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="28.6606" y="25.5786" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="U$5" gate="G$1" x="65" y="-4" smashed="yes" rot="R180"/>
<instance part="U$6" gate="G$1" x="7.5" y="-4" smashed="yes"/>
<instance part="U$7" gate="G$1" x="73" y="-13" smashed="yes"/>
<instance part="GND1" gate="G$1" x="3.5" y="-27.6" smashed="yes">
<attribute name="VALUE" x="3.5" y="-29.378" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND2" gate="G$1" x="-5" y="-4.6" smashed="yes">
<attribute name="VALUE" x="-5" y="-6.378" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND3" gate="G$1" x="43.2" y="-19.8" smashed="yes">
<attribute name="VALUE" x="43.8" y="-24.422" size="1.778" layer="96" rot="R180" align="top-center"/>
</instance>
<instance part="GND4" gate="G$1" x="77" y="13" smashed="yes">
<attribute name="VALUE" x="77" y="11.222" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND5" gate="G$1" x="65" y="5" smashed="yes">
<attribute name="VALUE" x="65" y="3.222" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND6" gate="G$1" x="93" y="-16.6" smashed="yes">
<attribute name="VALUE" x="93" y="-18.378" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-153" y="-73" smashed="yes">
<attribute name="DRAWING_NAME" x="191.17" y="-57.76" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="191.17" y="-62.84" size="2.286" layer="94"/>
<attribute name="SHEET" x="204.505" y="-67.92" size="2.54" layer="94"/>
</instance>
<instance part="1N5820" gate="G$1" x="3.5" y="-20.2" smashed="yes" rot="R90"/>
<instance part="1572-1039" gate="G$1" x="-5" y="12.08" smashed="yes" rot="R270"/>
<instance part="U$3" gate="G$1" x="43.2" y="-9.93" smashed="yes" rot="R270"/>
<instance part="CF18" gate="G$1" x="79.9" y="-4" smashed="yes"/>
<instance part="U$1" gate="G$1" x="-11" y="25" smashed="yes" rot="R90"/>
<instance part="GND7" gate="G$1" x="-19" y="22" smashed="yes">
<attribute name="VALUE" x="-19" y="20.222" size="1.778" layer="96" align="top-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<pinref part="GND1" gate="G$1" pin="3.3V"/>
<pinref part="1N5820" gate="G$1" pin="P$1"/>
<wire x1="3.5" y1="-25.06" x2="3.5" y2="-22.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="GND"/>
<pinref part="GND5" gate="G$1" pin="3.3V"/>
<wire x1="64.5" y1="13.92" x2="65" y2="13.92" width="0.1524" layer="91"/>
<wire x1="65" y1="13.92" x2="65" y2="7.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CF18" gate="G$1" pin="P$2"/>
<pinref part="U$7" gate="G$1" pin="P$2"/>
<wire x1="88.9" y1="-4" x2="88.9" y2="-13" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-13" x2="82" y2="-13" width="0.1524" layer="91"/>
<pinref part="GND6" gate="G$1" pin="3.3V"/>
<wire x1="88.9" y1="-4" x2="93" y2="-4" width="0.1524" layer="91"/>
<wire x1="93" y1="-4" x2="93" y2="-14.06" width="0.1524" layer="91"/>
<junction x="88.9" y="-4"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="~ON~/OFF"/>
<pinref part="GND4" gate="G$1" pin="3.3V"/>
<wire x1="64.5" y1="19" x2="77" y2="19" width="0.1524" layer="91"/>
<wire x1="77" y1="19" x2="77" y2="15.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<pinref part="GND7" gate="G$1" pin="3.3V"/>
<wire x1="-11" y1="30" x2="-19" y2="30" width="0.1524" layer="91"/>
<wire x1="-19" y1="30" x2="-19" y2="24.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="1572-1039" gate="G$1" pin="P$2"/>
<pinref part="GND2" gate="G$1" pin="3.3V"/>
<wire x1="-5" y1="4.08" x2="-5" y2="-2.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="G$1" pin="3.3V"/>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<wire x1="43.2" y1="-17.26" x2="43.2" y2="-15.93" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<pinref part="U1" gate="A" pin="OUT"/>
<wire x1="3.5" y1="-4" x2="3.5" y2="16.46" width="0.1524" layer="91"/>
<wire x1="3.5" y1="16.46" x2="3.54" y2="16.46" width="0.1524" layer="91"/>
<pinref part="1N5820" gate="G$1" pin="P$2"/>
<wire x1="3.5" y1="-10.2" x2="3.5" y2="-4" width="0.1524" layer="91"/>
<junction x="3.5" y="-4"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<pinref part="U$5" gate="G$1" pin="P$2"/>
<wire x1="19.5" y1="-4" x2="43.2" y2="-4" width="0.1524" layer="91"/>
<pinref part="U$7" gate="G$1" pin="P$1"/>
<wire x1="43.2" y1="-4" x2="55" y2="-4" width="0.1524" layer="91"/>
<wire x1="55" y1="-4" x2="55" y2="-13" width="0.1524" layer="91"/>
<wire x1="55" y1="-13" x2="64" y2="-13" width="0.1524" layer="91"/>
<junction x="55" y="-4"/>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<wire x1="43.2" y1="-4.93" x2="43.2" y2="-4" width="0.1524" layer="91"/>
<junction x="43.2" y="-4"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="P$1"/>
<pinref part="CF18" gate="G$1" pin="P$1"/>
<wire x1="69" y1="-4" x2="72.4" y2="-4" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="FB"/>
<wire x1="72.4" y1="-4" x2="75.9" y2="-4" width="0.1524" layer="91"/>
<wire x1="64.5" y1="16.46" x2="72.4" y2="16.46" width="0.1524" layer="91"/>
<wire x1="72.4" y1="16.46" x2="72.4" y2="-4" width="0.1524" layer="91"/>
<junction x="72.4" y="-4"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="A" pin="VIN"/>
<wire x1="-11" y1="19" x2="-5" y2="19" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<pinref part="1572-1039" gate="G$1" pin="P$1"/>
<wire x1="-5" y1="19" x2="3.54" y2="19" width="0.1524" layer="91"/>
<wire x1="-5" y1="19" x2="-5" y2="15.08" width="0.1524" layer="91"/>
<junction x="-5" y="19"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,64.5,13.92,U1,GND,3.3V,,,"/>
<approved hash="113,1,44.586,57.071,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
