## Name
India Spott

##cruzID
espott

##Date
11/7/2021

## Files in Repository
 espott-functionalBuckConverter.s#1
 espott-functionalBuckConverter.s#2
 espott-functionlBuckConverter.s#3
 espott-functionalBuckConverter.sch
 README
 libraries
     Lab6_Library
     LM2576T
     Lab6_Library.l#1
     Lab6_Library.l#2
     Lab6_Library.l#3
     Lab6_Library.l#4
     Lab6_Library.l#5
     Lab6_Library.l#6
     Lab6_Library.l#7
     Lab6_Library.l#8
     Lab6_Library.l#9

BoM Google Sheets Share Link: https://docs.google.com/spreadsheets/d/1CcDzGu6hb7Kh-BpFVTtj2f0lCb3vjnT_V4lrse74Heg/edit?usp=sharing
##Project Function
The project function is to make a functional buck converter schematic along with creating and sourcing our own parts and footprints.
The schematic is of a DC to DC buck converter that has an input of 12V and an output of 9V.
